$(document).ready(function () {
    //show menu
    $('.navbar-toggler').on('click',function () {
        $('.navbar-collapse').toggleClass('is-show');
        $(this).toggleClass('active');
    });
    $('.navbar-collapse .close').on('click',function () {
        $('.navbar-collapse').removeClass('is-show');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"']").addClass('active');

    $('.faq-list a').on('click',function (){
       let id = $(this).attr('href');
        $('html,body').animate({scrollTop: $(id).offset().top - 80},600);
    });

    $('.box-select .selected').on('click',function (){
        $(this).parent().toggleClass('show');
    });
    $('.select_list li').on('click',function (){
        let flag = $(this).data('flag');
        let country = $(this).data('country');
        let selected = `<div class="flag"><img src="${flag}" alt=""></div> <div class="country">${country}</div>`;
        $(this).parents('.box-select').find('.selected').html(selected);
        $('.box-select').removeClass('show');
    });

    $(document).mouseup(function (e) {
        var form_group = $('.box-select');
        if (!form_group.is(e.target) && form_group.has(e.target).length === 0) {
            $('.box-select').removeClass('show');
        }
    });


    // js effect_border
    $('.effect_border').on('click',function (){
        $(this).toggleClass('focus');
    })
    $('.effect_border').keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            $('.effect_border').removeClass('focus');
            $(this).next().addClass('focus');
        }
    });
    var item_input = $('.effect_border .form-control');
    $(item_input).on('change', function () {
        if ($(this).val().length > 0) {
            $(this).parents('.effect_border').addClass('filled'); 
        } else {
            $(this).parents('.effect_border').removeClass('filled');
        }
    });
    $(item_input).on('focus', function () {
        $(this).parents('.effect_border').addClass('focus');
    });

    var item_select = $('.effect_border select');

    $(item_select).on('change', function () {
        $(this).parents('.effect_border').addClass('filled');
    });

    $(document).mouseup(function (e) {
        var form_group = $('wrapper');
        if (!form_group.is(e.target) && form_group.has(e.target).length === 0) {
            $('.effect_border,.select_b').removeClass('focus');
        }
    });
});
